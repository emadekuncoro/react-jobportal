import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Jobs from '../containers/Jobs'
import Cms from '../containers/Cms'

const Main = () => (
  <main>
    <Switch>
      <Route exact path='/jobs' component={Jobs}/>
      <Route path='/cms' component={Cms}/>
    </Switch>
  </main>
)

export default Main
