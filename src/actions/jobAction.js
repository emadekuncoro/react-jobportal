import fetch from 'cross-fetch';


export function fetchJobs(data) {
    return dispatch => {
        let api_url = 'https://private-27298f-frontendtestmaukerja.apiary-mock.com/jobs';
        fetch(api_url)
        .then((resp) => {
            return resp.json();
        })
        .then((resp) => {
            
            if (resp.length > 0) {
                dispatch({
                    type: 'FETCH_JOBS',
                    payload: resp
                })
            }

        });
    }
};


export function fetchDetails(data) {
    return dispatch => {
        let api_url = 'https://private-27298f-frontendtestmaukerja.apiary-mock.com/job/'+data;
        fetch(api_url)
        .then((resp) => {
            return resp.json();
        })
        .then((resp) => {
            dispatch({
                type: 'FETCH_DETAILS',
                payload: resp
            })
        });
    }
};


export function insertJob(data) {
    return dispatch => {
        let api_url = 'https://private-27298f-frontendtestmaukerja.apiary-mock.com/jobs';
        fetch(api_url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
            },
            credentials: 'same-origin',
            body: JSON.stringify(data)
        })
        .then((resp) => {
            return resp.json();
        })
        .then((resp) => {
            dispatch({
                type: 'INSERT_JOB',
                payload: resp
            })
        });
    }
};


export function updateJob(data) {
    return dispatch => {
        let api_url = 'https://private-27298f-frontendtestmaukerja.apiary-mock.com/job/'+data;
        fetch(api_url)
        .then((resp) => {
            return resp.json();
        })
        .then((resp) => {
            dispatch({
                type: 'UPDATE_JOB',
                payload: resp
            })
        });
    }
};


export function deleteJob(data) {
    return dispatch => {
        let api_url = 'http://private-27298f-frontendtestmaukerja.apiary-mock.com/job/'+data;
        fetch(api_url, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
            }
        })
        .then((resp) => {
            return resp.json();
        })
        .then((resp) => {
            dispatch({
                type: 'DELETE_JOB',
                payload: resp
            })
        });
    }
};