import { createStore, combineReducers, applyMiddleware } from "redux";
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import job from "./reducers/jobReducer";

const loggerMiddleware = createLogger()

export default createStore(
    combineReducers({
        job
    }),
    {},
    applyMiddleware(
        thunkMiddleware, // lets us dispatch() functions
        loggerMiddleware // neat middleware that logs actions
    )
);