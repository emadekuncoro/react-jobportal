import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
//setup redux
import { connect } from "react-redux";
import { fetchJobs, fetchDetails } from "../actions/jobAction";

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  card: {
    margin: theme.spacing.unit * 2
  }
});

class Jobs extends React.Component{

  componentDidMount(){
      this.props.fetchJobs();
  }

  showDetails(param, e){
    this.props.fetchDetails(param);
  }

render() {
  const { classes } = this.props;
  let items = [];
  this.props.jobs.forEach((job, i) => {
      items.push(
        <Card id={job.id} className={classes.card}>
        <CardContent>
          <Typography className={classes.title} color="textSecondary" gutterBottom>
            {job.title}
          </Typography>
        
          <Typography className={classes.pos} color="textSecondary">
            {job.location}
          </Typography>
          <Typography component="p">
            {job.company}
          </Typography>
        </CardContent>
        <CardActions>
          <Button onClick = {this.showDetails.bind(this, job.id)} variant="contained" color="primary" className={classes.button} size="small">See Details</Button>
        </CardActions>
      </Card>
      
      );
  });

  let details = 
  <Paper className={classes.paper}>
  Job Details
  </Paper>

  if(this.props.details.title){
    let language = '';
    let items = '';
    if(this.props.details.requirements){
      language = this.props.details.requirements.language;
      items = this.props.details.requirements.items;
    }
    
    details = 
    <Paper className={classes.paper}>
            <h3>{this.props.details.title}</h3>
            <h4>{this.props.details.company}</h4>
            <p>{this.props.details.location}</p>
            <Divider />
            <Paper className={classes.root} elevation={1}>
              
              <Typography component="p">
                {this.props.details.description}
                <Divider />
                <h4>requirements</h4>
                {language}
                <br />
                {items}
                <Divider />
                <h4>Responsibilites</h4>
                {this.props.details.responsibilites}
                <Divider />
                <h4>Benefit</h4>
                {this.props.details.benefit}
              </Typography>
            </Paper>
          </Paper>
  }

  return (
    <div className={classes.root}>
      <Grid container spacing={24}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>search input here</Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper className={classes.paper}>
          <h4>Job List</h4>
          {items}
          </Paper>
        </Grid>
        <Grid item xs={8}>
          {details}
        </Grid>
        <Grid item xs={12}>
          <Paper className={classes.paper}>paging here</Paper>
        </Grid>
      </Grid>
    </div>
  );
}

}
Jobs.propTypes = {
  classes: PropTypes.object.isRequired,
};


const mapStateToProps = (state) => {
    return {
        jobs: state.job.data,
        details : state.job.details
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchJobs: (params) => {
            dispatch(fetchJobs(params));
        },
        fetchDetails: (params) => {
          dispatch(fetchDetails(params));
      }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Jobs));