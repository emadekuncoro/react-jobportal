import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
//setup redux
import { connect } from "react-redux";
import { fetchJobs, insertJob, fetchDetails } from "../actions/jobAction";

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class Cms extends React.Component{

  constructor(props) {
      super(props)
      this.state = {
          title: '',
          company: '',
          location: '',
          type: '',
          description: '',
          employer_contact:'',
          employer_phone: '',
          employer_email: '',
          requirements: '',
          requirement_language: '',
          requirement_items: '',
          responsibilities: '',
          benfit: '',
          nationality:'',
          isPanelShow: false,
      }
      this.handleSubmit = this.handleSubmit.bind(this)
      this.toggleForm = this.toggleForm.bind(this)
      this.handleChange = this.handleChange.bind(this)
      this.handleEdit = this.handleEdit.bind(this)
      this.handleDelete = this.handleDelete.bind(this)
  }
    componentDidMount(){
      document.getElementById("formPanel").hidden = true;
      this.props.fetchJobs();
      console.log(this.props)
    }

    componentWillReceiveProps(){
      let data = {
        title: this.props.details.title,
        company: this.props.details.company,
        location: this.props.details.location,
        type:this.props.details.type,
        description:this.props.details.description,
        employer_contact: {
          phone:this.props.details.employer_phone,
          whatsapp:601381333,
          email:this.props.details.employer_email
        },
        requirements: {
          language:this.props.details.requirement_language,
          items: this.props.details.requirement_items
        },
        responsibilites: this.props.details.responsibilities,
        benefit: this.props.details.benfit,
        nationality:'-',
        views:0
    } 
      this.setState(data);
    }

    handleSubmit(event) {
          event.preventDefault()
          var data = {
            "title": this.state.title,
            "company": this.state.company,
            "location": this.state.location,
            "type":this.state.type,
            "description":this.state.description,
            "employer_contact": {
              "phone":this.state.employer_phone,
              "whatsapp":"601381333",
              "email":this.state.employer_email
            },
            "requirements": {
              "language":this.state.requirement_language,
              "items": this.state.requirement_items
            },
            "responsibilites": this.state.responsibilities,
            "benefit": this.state.benfit,
            "nationality":"Malaysia",
            "views":0
        } 
          // console.log(data)
          this.props.insertJob(data);
          if(this.props.isInserted){
              alert('success insert job')
          }
    }

    toggleForm(e){
      if(!this.state.isPanelShow){
        document.getElementById("formPanel").hidden = false;
        this.setState({isPanelShow:true});
      }else{
        document.getElementById("formPanel").hidden = true;
        this.setState({isPanelShow:false});
      }
    }

    handleChange(e) {
      this.setState({[e.target.name]: e.target.value});  
    }
    handleEdit(param, e) {
      this.setState({isPanelShow:false});
      document.getElementById("formPanel").hidden = false;
      this.props.fetchDetails(param);
    }
    handleDelete(param, e) {
      this.props.deleteJob(param)
    }

   

render() {
   
    const {classes} = this.props;
    let items = [];
    this.props.jobs.forEach((job, i) => {
        items.push(
            <TableRow key={job.id}>
            <TableCell component="th" scope="row">
            {job.title}
            </TableCell>
            <TableCell>{job.company}</TableCell>
            <TableCell>{job.location}</TableCell>
            <TableCell><EditIcon onClick={this.handleEdit.bind(this, job.id)} className={classes.icon} /> <DeleteIcon onClick={this.handleDelete.bind(this, job.id)} className={classes.icon} /> </TableCell>
        </TableRow>

        );
    });
  return (
    <div className={classes.root}>
      <Grid container spacing={24}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>search input here</Paper>
        </Grid>

        <Grid id="formPanel" item xs={12}>
          <Paper className={classes.paper}>
            <form className={classes.container} onSubmit={this.handleSubmit} method="POST" autoComplete="off">
                <TextField
                id="standard-name"
                label="Job Title"
                name="title"
                className={classes.textField}
                value={this.state.title}
                onChange={this.handleChange}
                fullWidth
                margin="normal"
                />
                <TextField
                id="standard-name"
                label="Company"
                name="company"
                className={classes.textField}
                value={this.state.company}
                onChange={this.handleChange}
                fullWidth
                margin="normal"
                />
                <TextField
                id="standard-name"
                label="Location"
                name="location"
                className={classes.textField}
                value={this.state.location}
                onChange={this.handleChange}
                fullWidth
                margin="normal"
                />
                 <TextField
                id="standard-name"
                label="Type"
                name="type"
                className={classes.textField}
                value={this.state.type}
                onChange={this.handleChange}
                fullWidth
                margin="normal"
                />
                 <TextField
                id="standard-name"
                label="Description"
                name="description"
                className={classes.textField}
                value={this.state.description}
                onChange={this.handleChange}
                fullWidth
                margin="normal"
                />
                <TextField
                id="standard-name"
                label="Employer Phone"
                name="employer_phone"
                className={classes.textField}
                value={this.state.employer_phone}
                onChange={this.handleChange}
                fullWidth
                margin="normal"
                />
                <TextField
                id="standard-name"
                label="Employer Email"
                name="employer_email"
                className={classes.textField}
                value={this.state.employer_email}
                onChange={this.handleChange}
                fullWidth
                margin="normal"
                />
                <TextField
                id="standard-name"
                label="Language Requirement"
                name="requirement_language"
                className={classes.textField}
                value={this.state.requirement_language}
                onChange={this.handleChange}
                fullWidth
                margin="normal"
                />
                <TextField
                id="standard-name"
                label="Items Requirements"
                name="requirement_items"
                className={classes.textField}
                value={this.state.requirement_items}
                onChange={this.handleChange}
                fullWidth
                margin="normal"
                />
                <TextField
                id="standard-name"
                label="Responsibilities"
                name="responsibilities"
                className={classes.textField}
                value={this.state.responsibilities}
                onChange={this.handleChange}
                fullWidth
                margin="normal"
                />
                <TextField
                id="standard-name"
                label="Benefit"
                name="benefit"
                className={classes.textField}
                value={this.state.benefit}
                onChange={this.handleChange}
                fullWidth
                margin="normal"
                />
              
                <Button variant="contained" color="primary" className={classes.button} type="submit">Save</Button>
                
               
            </form>
          </Paper>
        </Grid>
     
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Button onClick={this.toggleForm} variant="contained" color="primary" className={classes.button}>
                New Data
            </Button>
          <Table className={classes.table}>
            <TableHead>
            <TableRow>
                <TableCell>Title</TableCell>
                <TableCell>Company</TableCell>
                <TableCell>Location</TableCell>
                <TableCell>#</TableCell>
            </TableRow>
            </TableHead>
            <TableBody>
           {items}
            </TableBody>
        </Table>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}

}
Cms.propTypes = {
  classes: PropTypes.object.isRequired,
};


const mapStateToProps = (state) => {
  return {
      jobs: state.job.data,
      isInserted: state.job.isInserted,
      details: state.job.details,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
      fetchJobs: () => {
          dispatch(fetchJobs());
      },
      insertJob: (data) => {
        dispatch(insertJob(data));
    },
    fetchDetails: (data) => {
      dispatch(fetchDetails(data));
  }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Cms));