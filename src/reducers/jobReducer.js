const jobReducer = (state = {
    data: [],
    details:{},
    isInserted:null,
}, action) => {
    switch (action.type) {
        case "FETCH_JOBS":
        state = {
            ...state,
            data: action.payload
        };
        break;
        case "FETCH_DETAILS":
        state = {
            ...state,
            details: action.payload
        };
        break;
        case "INSERT_JOB":
        state = {
            ...state,
            isInserted: action.payload.message
        };
        break;
    }
    return state;
};

export default jobReducer;